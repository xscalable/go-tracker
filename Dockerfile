FROM golang:1-alpine AS builder

WORKDIR /src

COPY ./go.mod ./go.sum ./

RUN apk add --no-cache git \
  && go get -d -v

COPY ./ ./

# LINUX X86_64
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags '-w -extldflags "-static"' -o /usr/local/bin/go-tracker-linux-amd64 *.go \
  # ARM7 (Raspberry Pi 3)
  && CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=7 go build -a -installsuffix cgo -ldflags '-w -extldflags "-static"' -o /usr/local/bin/go-tracker-linux-armhf *.go \
  # ARM8/64 (Odroid C2)
  && CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -a -installsuffix cgo -ldflags '-w -extldflags "-static"' -o /usr/local/bin/go-tracker-linux-aarch64 *.go

# run with --target=linux-amd64
FROM scratch AS linux-amd64
COPY --from=builder /usr/local/bin/go-tracker-linux-amd64 /go-tracker-linux-amd64
ENTRYPOINT ["./go-tracker-linux-amd64"]

# run with --target=linux-armhf
FROM scratch AS linux-armhf
COPY --from=builder /usr/local/bin/go-tracker-linux-armhf /go-tracker-linux-armhf
ENTRYPOINT ["./go-tracker-linux-armhf"]

# run with --target=linux-aarch64
FROM scratch AS linux-aarch64
COPY --from=builder /usr/local/bin/go-tracker-linux-aarch64 /go-tracker-linux-aarch64
ENTRYPOINT ["./go-tracker-linux-aarch64"]

# default build image
FROM linux-amd64
