module gitlab.com/xscalable/go-tracker

go 1.12

require (
	github.com/dimiro1/banner v0.0.0-20161108151223-c2f858997d49
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/gobuffalo/packr v1.22.0
	github.com/golang/protobuf v1.2.0
	github.com/google/pprof v0.0.0-20190228041337-2ef8d84b2e3c // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/gommon v0.2.8 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/stretchr/testify v1.3.0
	github.com/valyala/fasttemplate v1.0.0 // indirect
	golang.org/x/arch v0.0.0-20190226203302-36aee92af9e8 // indirect
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3
	google.golang.org/appengine v1.2.0 // indirect
	google.golang.org/grpc v1.19.0
	gopkg.in/go-playground/validator.v9 v9.27.0 // indirect
)
