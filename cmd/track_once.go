package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/go-tracker/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// helloCmd represents the hello command
var trackOnce = &cobra.Command{
	Use:   "track-once",
	Short: "fetch single position for a trackID",
	Long:  `This subcommand fetch a single position for a trackID`,
	Run: func(cmd *cobra.Command, args []string) {
		trackID := viper.GetString("track-once.trackID")
		fmt.Printf("Requesting single location for trackID %s\n", trackID)
		conn, err := connect()
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		c := pb.NewTrackerClient(conn)
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		evt, err := c.TrackOnce(ctx, &pb.TrackOnceRequest{TrackID: trackID})
		if err != nil {
			log.Fatalf("%v.TRACKONCE(_) = _, %v", c, err)
		}
		jsonEvent, _ := json.Marshal(evt)
		fmt.Printf("%s\n", jsonEvent)
	},
}

func connect() (*grpc.ClientConn, error) {
	tlsCacheDir := "./certs/"
	certFile := tlsCacheDir + "self-signed-cert.pem"
	creds, err := credentials.NewClientTLSFromFile(certFile, "xScalable")
	if err != nil {
		log.Fatal(err)
	}
	grpcServerListeningAddr := viper.GetString("grpc.addr")
	return grpc.Dial(grpcServerListeningAddr, grpc.WithTransportCredentials(creds))
}

func init() {
	rootCmd.AddCommand(trackOnce)
	trackOnce.Flags().String("trackID", "864180033487245", "TrackID")
	viper.BindPFlag("track-once.trackID", trackOnce.Flags().Lookup("trackID"))
}
