package gps103

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GPS103(t *testing.T) {
	assert := assert.New(t)

	validIMEI := "864180033487245"
	invalidIMEI := "123a123"
	protocolError := errors.New("unknown protocol")

	tests := []struct {
		packet   []byte
		resp     []byte
		err      error
		loggedIn bool
		imei     string
	}{
		{[]byte("invalid"), nil, protocolError, false, ""},
		{[]byte(fmt.Sprintf("##,imei:%s,A;", invalidIMEI)), nil, protocolError, false, ""},
		{[]byte(fmt.Sprintf("##,imei:%s,A;", validIMEI)), []byte("LOAD"), nil, true, validIMEI},
	}
	for i, test := range tests {
		gps103 := &GPS103{}
		resp, err := gps103.ProcessPacket(test.packet)
		assert.Equal(test.resp, resp, fmt.Sprintf("%v response should match -> %s", i, string(test.packet)))
		assert.Equal(test.err, err, "error should match")
		assert.Equal(test.loggedIn, gps103.LoggedIn(), fmt.Sprintf("%v loggedIn state should match", i))
		assert.Equal(test.imei, gps103.IMEI())
		connectedSince := gps103.ConnectedSince()
		lastHeard := gps103.LastHeard()
		if !gps103.LoggedIn() {
			assert.Nil(connectedSince)
			assert.Nil(lastHeard)
		} else {
			assert.NotNil(connectedSince)
			assert.NotNil(lastHeard)
			if connectedSince != nil && lastHeard != nil {
				assert.Equal(*connectedSince, *lastHeard)
			}
		}
	}
}
