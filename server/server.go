package server

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"

	"github.com/golang/protobuf/ptypes/empty"
	log "github.com/sirupsen/logrus"
	"gitlab.com/xscalable/go-tracker/pb"
	"gitlab.com/xscalable/go-tracker/types"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	autoTLS     = false
	tlsCacheDir = "./certs/"
	tlsHosts    = "*"
)

type Server struct {
	types.Gateway
}

func (s *Server) TrackOnce(ctx context.Context, in *pb.TrackOnceRequest) (*pb.TrackOnceResponse, error) {

	id, c := s.Gateway.SubscribeTrackerResponses()
	defer s.Gateway.UnsubscribeTrackerResponses(id)
	if err := s.Gateway.TrackOnce(in.TrackID); err != nil {
		return nil, err
	}

	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case trackerResponse := <-c:
			if trackerResponse.GetTrackID() == in.TrackID {
				return &pb.TrackOnceResponse{Track: trackerResponse}, nil
			}
		}
	}
}

func (s *Server) Subscribe(in *empty.Empty, stream pb.Tracker_SubscribeServer) error {
	ctx := stream.Context()

	trackerResponsesSubscriptionID, trackerResponses := s.Gateway.SubscribeTrackerResponses()
	defer s.Gateway.UnsubscribeTrackerResponses(trackerResponsesSubscriptionID)

	statusUpdatesSubscriptionID, statusUpdates := s.Gateway.SubscribeStatusUpdates()
	defer s.Gateway.UnsubscribeStatusUpdates(statusUpdatesSubscriptionID)
	messages := &pb.Subscription{
		Messages: []*pb.SubscriptionMessage{
			&pb.SubscriptionMessage{Message: &pb.SubscriptionMessage_CurrentLocations{CurrentLocations: s.Gateway.GetCurrentLocations()}},
			&pb.SubscriptionMessage{Message: &pb.SubscriptionMessage_CurrentStatus{CurrentStatus: s.Gateway.GetCurrentStatus()}},
		},
	}
	if err := stream.Send(messages); err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case trackerResponse := <-trackerResponses:
			messages := &pb.Subscription{
				Messages: []*pb.SubscriptionMessage{
					&pb.SubscriptionMessage{Message: &pb.SubscriptionMessage_Location{Location: trackerResponse}},
				},
			}
			if err := stream.Send(messages); err != nil {
				return err
			}
		case statusUpdate := <-statusUpdates:
			messages := &pb.Subscription{
				Messages: []*pb.SubscriptionMessage{
					&pb.SubscriptionMessage{Message: &pb.SubscriptionMessage_Status{Status: statusUpdate}},
				},
			}
			if err := stream.Send(messages); err != nil {
				return err
			}
		}
	}
}

func Run(gw types.Gateway, address string) error {
	var creds credentials.TransportCredentials
	var err error

	// log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{})

	if !autoTLS {
		hostWhiteList := strings.Split(tlsHosts, " ")
		// certFile := config.API.TLSCacheDir + "self-signed-cert.pem"
		// keyFile := config.API.TLSCacheDir + "self-signed-cert-key.pem"
		certFile := tlsCacheDir + "self-signed-cert.pem"
		keyFile := tlsCacheDir + "self-signed-cert-key.pem"
		certificateExists := true
		if _, err := os.Stat(certFile); os.IsNotExist(err) {
			certificateExists = false
		}
		if _, err := os.Stat(keyFile); os.IsNotExist(err) {
			certificateExists = false
		}

		if !certificateExists {
			log.Infof("Generating self-signed certificate\nHosts: %s\nCache directory: %s", tlsHosts, tlsCacheDir)

			x509KeyPair, err := generateSelfSignedCertificate(hostWhiteList...)
			if err != nil {
				log.Fatalf("Failed to create certificate: %s", err)
			}

			if err := ioutil.WriteFile(certFile, []byte(x509KeyPair.Certificate), 0644); err != nil {
				log.Fatalf("Failed to save certificate: %s", err)
			}
			if err := ioutil.WriteFile(keyFile, []byte(x509KeyPair.Key), 0644); err != nil {
				log.Fatalf("Failed to save certificate key: %s", err)
			}
		}

		creds, err = credentials.NewServerTLSFromFile(certFile, keyFile)
		if err != nil {
			log.Fatal(err)
		}
	}
	s := grpc.NewServer(grpc.Creds(creds))
	pb.RegisterTrackerServer(s, &Server{Gateway: gw})

	lis, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("failed to listen: %v", err)
	}

	log.Infof("grpc listening: %s", address)

	if err := s.Serve(lis); err != nil {
		return fmt.Errorf("failed to serve: %v", err)
	}
	return nil
}
