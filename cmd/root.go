package cmd

import (
	"bytes"
	"fmt"
	"os"

	"github.com/dimiro1/banner"
	"github.com/gobuffalo/packr"
	colorable "github.com/mattn/go-colorable"

	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/go-tracker/gateway"
	"gitlab.com/xscalable/go-tracker/server"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "go-tracker",
	Short: "Go Tracker",
	Long:  `Go Tracker`,

	Run: func(cmd *cobra.Command, args []string) {
		trackerGWListeningAddr := viper.GetString("tracker.addr")
		grpcServerListeningAddr := viper.GetString("grpc.addr")
		gw := gateway.NewGateway(trackerGWListeningAddr)
		go func() {
			gw.Run()
		}()
		if err := server.Run(gw, grpcServerListeningAddr); err != nil {
			log.Error(err)
		}
	},
}

func Execute() {
	showBanner()
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func showBanner() {
	box := packr.NewBox("../resources")
	bannerString, err := box.FindString("banner.txt")
	if err == nil {
		banner.Init(colorable.NewColorableStdout(), true, true, bytes.NewBufferString(bannerString))
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.go_tracker.yml)")

	rootCmd.PersistentFlags().StringP("tracker_addr", "t", ":6969", "tracker gateway listening address")
	rootCmd.PersistentFlags().StringP("grpc_addr", "g", ":50051", "grpc server listening address")

	viper.BindPFlag("tracker.addr", rootCmd.PersistentFlags().Lookup("tracker_addr"))
	viper.BindPFlag("grpc.addr", rootCmd.PersistentFlags().Lookup("grpc_addr"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in both current and home directory with name ".go-tracker" (without extension).
		viper.SetConfigName(".go-tracker")
		viper.AddConfigPath(".")
		viper.AddConfigPath(home)
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("go_tracker")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
