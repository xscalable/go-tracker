# Go parameters
GONAME=$(shell basename "$(PWD)")
LOCALBRANCH=$(shell git branch | grep \* | cut -d ' ' -f2)

ifndef CONTAINER_IMAGE
override CONTAINER_IMAGE = registry.gitlab.com/xscalable/go-tracker
endif

all: clean build

generate:
	go generate

watch:
	modd

build: clean build-dev

build-dev:
	packr build -mod=vendor -ldflags "-w -s -extldflags "-static" -X main.environment=development -X main.buildstamp=`date -I'seconds'` -X main.githash=`git rev-parse HEAD` -X main.version=`git branch | grep \* | cut -d ' ' -f2`" -o $(GONAME).exe

build-all: build-windows build-linux build-aarch64 build-armhf

test: 
	go test -v ./...

clean: 
	go clean
	rm -f $(GONAME)
	rm -f $(GONAME).exe
	rm -rf bin/*

run:
	packr build -o $(GONAME) -v ./...
	./$(GONAME)

deps:
	go mod vendor

# Cross compilation

build-windows:
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 packr build -mod=vendor -a -installsuffix cgo -ldflags "-w -s -extldflags "-static" -X main.buildstamp=`date -I'seconds'` -X main.githash=`git rev-parse HEAD` -X main.version=`git branch | grep \* | cut -d ' ' -f2`" -o bin/$(GONAME)-windows-amd64

build-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 packr build -mod=vendor -a -installsuffix cgo -ldflags "-w -s -extldflags "-static" -X main.buildstamp=`date -I'seconds'` -X main.githash=`git rev-parse HEAD` -X main.version=`git branch | grep \* | cut -d ' ' -f2`" -o bin/$(GONAME)-linux-amd64

build-aarch64:
	CGO_ENABLED=0 GOOS=linux GOARCH=arm64 packr build -mod=vendor -a -installsuffix cgo -ldflags "-w -s -extldflags "-static" -X main.buildstamp=`date -I'seconds'` -X main.githash=`git rev-parse HEAD` -X main.version=`git branch | grep \* | cut -d ' ' -f2`" -o bin/$(GONAME)-linux-aarch64

build-armhf:
	CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=7 packr build -mod=vendor -a -installsuffix cgo -ldflags "-w -s -extldflags "-static" -X main.buildstamp=`date -I'seconds'` -X main.githash=`git rev-parse HEAD` -X main.version=`git branch | grep \* | cut -d ' ' -f2`" -o bin/$(GONAME)-linux-armhf

build-docker:
	docker build -f Dockerfile.release --target=linux-amd64 -t registry.gitlab.com/xscalable/users:$(LOCALBRANCH) .
