package helpers

import (
	"fmt"
	"strconv"
	"strings"
)

func DMSToDecimalCoords(dms string, easting string) (string, error) {
	eastingFactor := map[string]float64{
		"E": 1,
		"W": -1,
		"N": 1,
		"S": -1,
	}
	var coord string
	splitIdx := -1
	ef, ok := eastingFactor[easting]
	if !ok {
		return coord, fmt.Errorf("invalid easting: got %s, possible values: E, W, N, S", easting)
	}
	for i, char := range dms {
		if char == '.' {
			splitIdx = i - 2
			break
		}
	}
	if splitIdx < 0 || splitIdx > len(dms) {
		return coord, fmt.Errorf("invalid DMS value: %s", dms)
	}

	degree, err := strconv.ParseInt(strings.TrimLeft(dms[0:splitIdx], "0"), 0, 32)
	if err != nil {
		return coord, err
	}
	ms, err := strconv.ParseFloat(dms[splitIdx:len(dms)], 32)
	if err != nil {
		return coord, err
	}

	return fmt.Sprintf("%.6f", ef*(float64(degree)+ms/60)), nil
}
