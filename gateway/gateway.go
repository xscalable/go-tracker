package gateway

import (
	"errors"
	"fmt"
	"io"
	"net"
	"sync"
	"time"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/xscalable/go-tracker/gateway/gps103"
	"gitlab.com/xscalable/go-tracker/pb"
	"gitlab.com/xscalable/go-tracker/types"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// NewServer creates a new Server using given protocol
// and addr.
func NewGateway(addr string) types.Gateway {
	gw := &TCPServer{
		addr:                       addr,
		publishers:                 map[string]types.Protocol{},
		trackerResponsePublishers:  map[string]chan *pb.TrackerResponse{},
		statusUpdatePublishers:     map[string]chan *pb.StatusUpdate{},
		trackerResponseSubscribers: map[string]chan *pb.TrackerResponse{},
		statusUpdateSubscribers:    map[string]chan *pb.StatusUpdate{},
	}
	return gw
}

// TCPServer holds the structure of our TCP
// implementation.
type TCPServer struct {
	addr                            string
	server                          net.Listener
	publishers                      map[string]types.Protocol
	publishersMutex                 sync.Mutex
	trackerResponsePublishers       map[string]chan *pb.TrackerResponse
	trackerResponsePublishersMutex  sync.Mutex
	statusUpdatePublishers          map[string]chan *pb.StatusUpdate
	statusUpdatePublishersMutex     sync.Mutex
	trackerResponseSubscribers      map[string]chan *pb.TrackerResponse
	trackerResponseSubscribersMutex sync.Mutex
	statusUpdateSubscribers         map[string]chan *pb.StatusUpdate
	statusUpdateSubscribersMutex    sync.Mutex
}

// Run starts the TCP Server.
func (t *TCPServer) Run() (err error) {
	t.server, err = net.Listen("tcp", t.addr)
	if err != nil {
		return err
	}
	defer t.Close()

	doneTrackerResponses := make(chan bool)
	defer close(doneTrackerResponses)

	doneStatusUpdates := make(chan bool)
	defer close(doneStatusUpdates)

	go t.publishTrackerResponses(doneTrackerResponses)
	go t.publishStatusUpdates(doneStatusUpdates)

	for {
		conn, err := t.server.Accept()
		if err != nil {
			err = errors.New("could not accept connection")
			break
		}
		if conn == nil {
			err = errors.New("could not create connection")
			break
		}
		go t.handleConnection(conn)
	}
	return
}

// Close shuts down the TCP Server
func (t *TCPServer) Close() (err error) {
	return t.server.Close()
}

func (t *TCPServer) handleConnection(conn net.Conn) {
	connID := xid.New().String()
	var wg sync.WaitGroup
	defer conn.Close()

	log.WithFields(log.Fields{
		"event": "connect",
		"id":    connID,
		"addr":  conn.RemoteAddr().String(),
	}).Info("client connected")

	defer log.WithFields(log.Fields{
		"event": "disconnect",
		"id":    connID,
		"addr":  conn.RemoteAddr().String(),
	}).Info("client disconnected")

	gps103, err := gps103.NewHandler(conn)
	if err != nil {
		log.Fatal(err)
	}

	wg.Add(3)

	go func() {
		defer wg.Done()
		t.publishersMutex.Lock()
		t.publishers[gps103.GetConnectionID()] = gps103
		t.publishersMutex.Unlock()
	}()

	go func() {
		defer wg.Done()
		t.trackerResponsePublishersMutex.Lock()
		t.trackerResponsePublishers[gps103.GetConnectionID()] = gps103.GetTrackerResponseChan()
		t.trackerResponsePublishersMutex.Unlock()
	}()

	go func() {
		defer wg.Done()
		t.statusUpdatePublishersMutex.Lock()
		t.statusUpdatePublishers[gps103.GetConnectionID()] = gps103.GetStatusUpdateChan()
		t.statusUpdatePublishersMutex.Unlock()
	}()

	wg.Wait()

	if err := gps103.Listen(); err != nil {
		if err != io.EOF {
			log.Error(err)
		}
	}

	wg.Add(3)

	go func() {
		defer wg.Done()
		t.publishersMutex.Lock()
		delete(t.publishers, gps103.GetConnectionID())
		t.publishersMutex.Unlock()
	}()

	go func() {
		defer wg.Done()
		t.trackerResponsePublishersMutex.Lock()
		delete(t.trackerResponsePublishers, gps103.GetConnectionID())
		t.trackerResponsePublishersMutex.Unlock()
	}()

	go func() {
		defer wg.Done()
		t.statusUpdatePublishersMutex.Lock()
		delete(t.statusUpdatePublishers, gps103.GetConnectionID())
		t.statusUpdatePublishersMutex.Unlock()
	}()

	wg.Wait()

}

func (t *TCPServer) publishTrackerResponses(done chan bool) {
Loop:
	for {
		t.trackerResponsePublishersMutex.Lock()
		for publisherID, publisher := range t.trackerResponsePublishers {
			select {
			case trackerResponse, ok := <-publisher:
				if !ok {
					// Channel is closed, remove it from trackingEventPublishers
					delete(t.trackerResponsePublishers, publisherID)
					return
				}
				t.trackerResponseSubscribersMutex.Lock()
				for _, subscriber := range t.trackerResponseSubscribers {
					subscriber <- trackerResponse
				}
				t.trackerResponseSubscribersMutex.Unlock()
			case <-time.After(time.Millisecond):
			}
		}
		t.trackerResponsePublishersMutex.Unlock()
		select {
		case <-time.After(500 * time.Millisecond):
		case <-done:
			break Loop
		}
	}
}

func (t *TCPServer) publishStatusUpdates(done chan bool) {
Loop:
	for {
		t.statusUpdatePublishersMutex.Lock()
		for publisherID, publisher := range t.statusUpdatePublishers {
			select {
			case statusUpdate, ok := <-publisher:
				if !ok {
					// Channel is closed, remove it from trackingEventPublishers
					delete(t.statusUpdatePublishers, publisherID)
					return
				}
				t.statusUpdateSubscribersMutex.Lock()
				for _, subscriber := range t.statusUpdateSubscribers {
					subscriber <- statusUpdate
				}
				t.statusUpdateSubscribersMutex.Unlock()
				if statusUpdate.Type == pb.StatusUpdateType_LOGGED_IN {
					t.publishersMutex.Lock()
					if p, ok := t.publishers[publisherID]; ok {
						go func() {
							time.Sleep(2 * time.Second)
							// set initial state after login
							if err := p.CancelTrack(); err != nil {
								log.Error(err)
							}
							if err := p.TrackByTime(pb.TrackingPeriod_s15); err != nil {
								log.Error(err)
							}
							if err := p.TrackByDistance(pb.TrackingDistance_m0050); err != nil {
								log.Error(err)
							}
							if err := p.Arm(); err != nil {
								log.Error(err)
							}
						}()
					}
					t.publishersMutex.Unlock()
				}
			case <-time.After(time.Millisecond):
			}
		}
		t.statusUpdatePublishersMutex.Unlock()
		select {
		case <-time.After(500 * time.Millisecond):
		case <-done:
			break Loop
		}
	}
}

func (t *TCPServer) GetCurrentStatus() *pb.CurrentStatus {
	currentStatus := map[string]*pb.StatusUpdate{}
	t.publishersMutex.Lock()
	defer t.publishersMutex.Unlock()
	for _, publisher := range t.publishers {
		statusUpdate := publisher.GetLastStatusUpdate()
		if statusUpdate != nil {
			currentStatus[publisher.GetTrackingID()] = statusUpdate
		}
	}
	return &pb.CurrentStatus{Status: currentStatus}
}

func (t *TCPServer) GetCurrentLocations() *pb.CurrentLocations {
	locations := map[string]*pb.TrackerResponse{}
	t.publishersMutex.Lock()
	defer t.publishersMutex.Unlock()
	for _, publisher := range t.publishers {
		trackerResponse := publisher.GetLastTrackerResponse()
		if trackerResponse != nil {
			locations[publisher.GetTrackingID()] = trackerResponse
		}
	}
	return &pb.CurrentLocations{Locations: locations}
}

func (t *TCPServer) SubscribeStatusUpdates() (id string, c chan *pb.StatusUpdate) {
	id = xid.New().String()
	c = make(chan *pb.StatusUpdate)
	t.statusUpdateSubscribersMutex.Lock()
	t.statusUpdateSubscribers[id] = c
	t.statusUpdateSubscribersMutex.Unlock()
	return id, c
}

func (t *TCPServer) UnsubscribeStatusUpdates(id string) {
	t.statusUpdateSubscribersMutex.Lock()
	if c, ok := t.statusUpdateSubscribers[id]; ok {
		close(c)
		delete(t.statusUpdateSubscribers, id)
	}
	t.statusUpdateSubscribersMutex.Unlock()
}

func (t *TCPServer) SubscribeTrackerResponses() (id string, c chan *pb.TrackerResponse) {
	id = xid.New().String()
	c = make(chan *pb.TrackerResponse)
	t.trackerResponseSubscribersMutex.Lock()
	t.trackerResponseSubscribers[id] = c
	t.trackerResponseSubscribersMutex.Unlock()
	return id, c
}

func (t *TCPServer) UnsubscribeTrackerResponses(id string) {
	t.trackerResponseSubscribersMutex.Lock()
	if c, ok := t.trackerResponseSubscribers[id]; ok {
		close(c)
		delete(t.trackerResponseSubscribers, id)
	}
	t.trackerResponseSubscribersMutex.Unlock()
}

func (t *TCPServer) TrackOnce(trackID string) error {
	err := status.Error(codes.NotFound, fmt.Sprintf("%s not found", trackID))
	t.publishersMutex.Lock()
	defer t.publishersMutex.Unlock()

	for _, publisher := range t.publishers {
		if publisher.GetTrackingID() == trackID {
			return publisher.TrackOnce()
		}
	}
	return err
}
