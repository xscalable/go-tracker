package gps103

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"regexp"
	"sync"
	"time"

	"github.com/golang/protobuf/ptypes"
	tspb "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/xscalable/go-tracker/helpers"
	"gitlab.com/xscalable/go-tracker/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	gps103login     *regexp.Regexp
	gps103heartbeat *regexp.Regexp
	gps103report    *regexp.Regexp
	gps103Message   *regexp.Regexp
)

const (
	chanBufLen = 100
)

func init() {
	gps103login = regexp.MustCompile("##,imei:(\\d{15}),A;")
	gps103heartbeat = regexp.MustCompile("^([0-9]{15});$")     // heartbeat packet consits of imei digits
	gps103report = regexp.MustCompile("^imei:(\\d{15}),(.+);") // Device report message format
	gps103Message = regexp.MustCompile("^imei:(?P<id>\\d+),(?P<keyword>\\w+),(?P<localTime>\\d*),(?P<phoneNumber>\\d*),(?P<gps>[FL]),(?P<utcTime>[0-9.]*),(?P<av>[AV]?),(?P<latitude>[0-9.abcdef]*),(?P<northing>[N-S]?),(?P<longitude>[0-9.abcdef]*),(?P<easting>[E-W]?),(?P<speed>[0-9.]*),(?P<heading>[0-9.]*);")
}

type GPS103 struct {
	id                       string
	conn                     net.Conn
	imei                     string
	loggedIn                 bool
	connectedSince           time.Time
	lastHeard                time.Time
	stalled                  bool
	stalledWatchdog          *helpers.Watchdog
	rw                       *bufio.ReadWriter
	statusUpdateChan         chan *pb.StatusUpdate
	trackerResponseChan      chan *pb.TrackerResponse
	responseSubscribers      map[string]chan (*pb.TrackerResponse)
	responseSubscribersMutex sync.RWMutex
	lastStatusUpdate         *pb.StatusUpdate
	lastTrackerResponse      *pb.TrackerResponse
}

func NewHandler(conn net.Conn) (*GPS103, error) {
	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
	gps103 := &GPS103{
		id:                  xid.New().String(),
		conn:                conn,
		rw:                  rw,
		statusUpdateChan:    make(chan *pb.StatusUpdate),
		trackerResponseChan: make(chan *pb.TrackerResponse),
		responseSubscribers: map[string]chan (*pb.TrackerResponse){},
	}
	return gps103, nil
}

func (p *GPS103) subscribeTrackResponses() (id string, c chan *pb.TrackerResponse) {
	id = xid.New().String()
	c = make(chan *pb.TrackerResponse)
	p.responseSubscribersMutex.Lock()
	defer p.responseSubscribersMutex.Unlock()
	p.responseSubscribers[id] = c
	return id, c
}

func (p *GPS103) unsubscribeTrackResponses(id string) {
	p.responseSubscribersMutex.Lock()
	defer p.responseSubscribersMutex.Unlock()
	delete(p.responseSubscribers, id)
}

func (p *GPS103) Listen() error {
	var err error

	if err := p.waitLogin(); err != nil {
		return err
	}

Loop:
	for {
		resp, _err := p.processPacket()
		if resp != nil && len(resp) > 0 {
			if _, _err := p.rw.Write(resp); _err != nil {
				err = _err
				break Loop
			}
			if _err := p.rw.Flush(); err != nil {
				err = _err
				break Loop
			}
		} else if _err != nil {
			if _err == io.EOF {
				fmt.Println("Client closed connection")
			} else {
				err = _err
			}
			break
		}
	}
	return err
}

func (p *GPS103) connect(imei string) {
	p.imei = imei
	p.loggedIn = true
	p.connectedSince = time.Now()
	p.lastHeard = time.Now()
	if p.stalledWatchdog != nil {
		p.stalledWatchdog.Kick()
	} else {
		p.stalledWatchdog = helpers.NewWatchdog(65*time.Second, p.stalledCallback) // Heartbeat period is 60s
	}

	log.WithFields(log.Fields{
		"id":       p.GetTrackingID(),
		"protocol": "gps103",
		"event":    "login",
	}).Info("device sucessfully registered")

	p.updateStatusChan(pb.StatusUpdateType_LOGGED_IN)
}

func (p *GPS103) heartbeat() {
	log.WithFields(log.Fields{
		"id":       p.GetTrackingID(),
		"protocol": "gps103",
		"event":    "heartbeat",
	}).Info("heartbeat received")

	p.stalledWatchdog.Kick()
	p.stalled = false
	p.lastHeard = time.Now()
	p.updateStatusChan(pb.StatusUpdateType_HEARTBEAT)
}

func (p *GPS103) disconnect() {
	defer close(p.statusUpdateChan)
	defer close(p.trackerResponseChan)
	p.stalledWatchdog.Stop()
	p.loggedIn = false
	p.updateStatusChan(pb.StatusUpdateType_LOGGED_OUT)

}

func (p *GPS103) stalledCallback() {
	p.stalled = true

	log.WithFields(log.Fields{
		"id":       p.GetTrackingID(),
		"protocol": "gps103",
		"event":    "stalled",
	}).Info("device stalled")

	p.updateStatusChan(pb.StatusUpdateType_STALLED)
}

func (p *GPS103) waitLogin() error {
	buffProbeTimer := time.NewTimer(500 * time.Millisecond)
	loginTimeoutTimer := time.NewTimer(5000 * time.Millisecond)
	defer buffProbeTimer.Stop()
	defer loginTimeoutTimer.Stop()

	for {
		select {
		case <-buffProbeTimer.C:
			// login packet must have, at least, 26 bytes
			buffer, err := p.rw.Peek(26)
			if err != nil {
				return err
			}
			if match := gps103login.FindStringSubmatch(string(buffer)); len(match) == 2 {
				_, err := p.rw.ReadString(';')
				if err != nil {
					return err
				}
				if _, err := p.rw.Write([]byte("LOAD")); err != nil {
					return err
				}
				if err := p.rw.Flush(); err != nil {
					return err
				}
				p.connect(match[1])
				return nil
			}
		case <-loginTimeoutTimer.C:
			return fmt.Errorf("login timeout")
		}
	}

}

func (p *GPS103) processPacket() ([]byte, error) {
	packet, err := p.rw.ReadString(';')
	if err != nil {
		if err == io.EOF {
			// client disconnected
			p.disconnect()
			return nil, err
		}
		return nil, err
	}

	log.WithFields(log.Fields{
		"id":       p.GetTrackingID(),
		"protocol": "gps103",
		"event":    "rx",
	}).Info(string(packet))

	if match := gps103heartbeat.FindStringSubmatch(packet); len(match) == 2 {
		imei := match[1]
		if p.imei != imei {
			p.disconnect()
			return nil, fmt.Errorf("imei mismatch")
		}
		p.heartbeat()
		return []byte("ON"), nil
	} else if match := gps103Message.FindStringSubmatch(packet); len(match) > 0 {
		result := make(map[string]string)
		for i, name := range gps103Message.SubexpNames() {
			if i != 0 && name != "" {
				result[name] = match[i]
			}
		}
		if result["id"] != p.imei {
			p.disconnect()
			return nil, fmt.Errorf("imei mismatch")
		}
		keyword := result["keyword"]
		var responseType pb.TrackerResponseType
		switch keyword {
		case "tracker", "001":
			responseType = pb.TrackerResponseType_LOCATION
		case "dt", "102":
			responseType = pb.TrackerResponseType_TRACKING_CANCELLED
		case "et":
			responseType = pb.TrackerResponseType_ALARM_CANCELLED
		case "gt":
			responseType = pb.TrackerResponseType_MOVEMENT_ALARM_CANCELLED
		case "it", "108":
			responseType = pb.TrackerResponseType_TIMEZONE_SET
		case "tt", "118":
			responseType = pb.TrackerResponseType_LESS_GPRS_MODE_ACTIVATED
		case "xt", "119":
			responseType = pb.TrackerResponseType_LESS_GPRS_MODE_DEACTIVATED
		case "lt", "111":
			responseType = pb.TrackerResponseType_ARMED
		case "511":
			responseType = pb.TrackerResponseType_ARMED_FAILED
		case "mt", "112":
			responseType = pb.TrackerResponseType_DISARMED
		case "help me":
			responseType = pb.TrackerResponseType_SOS_ALARM
		case "low battery":
			responseType = pb.TrackerResponseType_LOW_BATTERY_ALARM
		case "move":
			responseType = pb.TrackerResponseType_MOVEMENT_ALARM
		case "speed":
			responseType = pb.TrackerResponseType_OVERSPEED_ALARM
		case "stockade":
			responseType = pb.TrackerResponseType_GEOFENCE_ALARM
		case "ac alarm":
			responseType = pb.TrackerResponseType_POWER_OFF_ALARM
		case "door alarm":
			responseType = pb.TrackerResponseType_DOOR_OPEN_ALARM
		case "sensor alarm":
			responseType = pb.TrackerResponseType_SHOCK_ALARM
		case "acc alarm":
			responseType = pb.TrackerResponseType_ACC_ALARM
		case "accident alarm":
			responseType = pb.TrackerResponseType_ACCIDENT_ALARM
		case "bonnet alarm":
			responseType = pb.TrackerResponseType_BONNET_ALARM
		case "footbrake alarm":
			responseType = pb.TrackerResponseType_FOOTBRAKE_ALARM
		case "T:":
			responseType = pb.TrackerResponseType_TEMPERATURE_ALARM
		case "oil":
			responseType = pb.TrackerResponseType_OIL_ALARM
		case "service":
			responseType = pb.TrackerResponseType_VEHICLE_MAINTENANCE_NOTIFICATION
		case "acc on":
			responseType = pb.TrackerResponseType_ACC_ON
		case "acc off":
			responseType = pb.TrackerResponseType_ACC_OFF
		default:
			return nil, fmt.Errorf("UNKNWON KEYWORD: %s", keyword)
		}

		var timestamp *tspb.Timestamp
		if result["gps"] == "F" {
			localTime, err := time.Parse("060102150405", result["localTime"])
			if err != nil {
				return nil, fmt.Errorf("Invalid date format: %s -> %s", result["localTime"], err)
			}
			utcTime, err := time.Parse("150405.000", result["utcTime"])
			if err != nil {
				return nil, fmt.Errorf("Invalid date format: %s -> %s", result["utcTime"], err)
			}
			localHr, localMins, _ := localTime.Clock()
			utcHr, utcMins, _ := utcTime.Clock()

			// timezone calculation
			deltaMins := (localHr-utcHr)*60 + localMins - utcMins
			if deltaMins <= -12*60 {
				deltaMins += 24 * 60
			} else if deltaMins > 12*60 {
				deltaMins -= 24 * 60
			}
			timestamp, err = ptypes.TimestampProto(localTime.Add(-time.Minute * time.Duration(deltaMins)))
			if err != nil {
				return nil, fmt.Errorf("Invalid timestamp conversion")
			}
		} else {
			timestamp = ptypes.TimestampNow()
		}

		var latitude string
		var longitude string
		var lac string
		var cid string

		if result["gps"] == "F" {
			latitude, err = helpers.DMSToDecimalCoords(result["latitude"], result["northing"])
			if err != nil {
				fmt.Printf("Invalid latitude field: %s -> %s", result["latitude"], err)
			}
			longitude, err = helpers.DMSToDecimalCoords(result["longitude"], result["easting"])
			if err != nil {
				fmt.Printf("Invalid longitude field: %s -> %s", result["longitude"], err)
			}
		} else {
			lac = result["latitude"]
			cid = result["longitude"]
		}

		trackerResponse := &pb.TrackerResponse{
			Type:      responseType,
			TrackID:   p.imei,
			Gps:       result["gps"],
			Timestamp: timestamp,
			Lat:       latitude,
			Long:      longitude,
			Alt:       result["altitude"],
			Speed:     result["speed"],
			Heading:   result["heading"],
			Lac:       lac,
			Cid:       cid,
		}

		p.updateTrackerResponseChan(trackerResponse)
		return nil, nil
	}

	return nil, fmt.Errorf("unknown protocol: %s", string(packet))
}

func (p *GPS103) GetConnectionID() string {
	return p.id
}

func (p *GPS103) GetTrackingID() string {
	return p.imei
}

func (p *GPS103) GetLastStatusUpdate() *pb.StatusUpdate {
	return p.lastStatusUpdate
}

func (p *GPS103) GetLastTrackerResponse() *pb.TrackerResponse {
	return p.lastTrackerResponse
}

func (p *GPS103) GetStatusUpdateChan() chan *pb.StatusUpdate {
	return p.statusUpdateChan
}

func (p *GPS103) GetTrackerResponseChan() chan *pb.TrackerResponse {
	return p.trackerResponseChan
}

func (p *GPS103) updateStatusChan(t pb.StatusUpdateType) {
	connectedSince, err := ptypes.TimestampProto(p.connectedSince)
	if err != nil {
		log.Error(err)
	}

	lastHeard, err := ptypes.TimestampProto(p.lastHeard)
	if err != nil {
		log.Error(err)
	}

	statusUpdate := &pb.StatusUpdate{
		Timestamp:      ptypes.TimestampNow(),
		Type:           t,
		TrackID:        p.imei,
		Protocol:       "gps103",
		RemoteAddr:     p.conn.RemoteAddr().String(),
		LoggedIn:       p.loggedIn,
		ConnectedSince: connectedSince,
		LastHeard:      lastHeard,
		Stalled:        p.stalled,
	}

	p.lastStatusUpdate = statusUpdate
	if len(p.statusUpdateChan) == chanBufLen {
		<-p.statusUpdateChan
		log.Warnf("gps103 statusUpdateChan overflow...\n")
	}
	p.statusUpdateChan <- statusUpdate
}

func (p *GPS103) updateTrackerResponseChan(trackerResponse *pb.TrackerResponse) {
	p.lastTrackerResponse = trackerResponse
	if len(p.trackerResponseChan) == chanBufLen {
		<-p.trackerResponseChan
		log.Warnf("gps103 trackerResponseChan overflow...\n")
	}
	p.trackerResponseChan <- trackerResponse
	p.responseSubscribersMutex.RLock()
	defer p.responseSubscribersMutex.RUnlock()
	for _, subscriber := range p.responseSubscribers {
		subscriber <- trackerResponse
	}
}

func (p *GPS103) sendCommand(command string, expectedResponseType pb.TrackerResponseType) error {
	if p.loggedIn == false {
		return fmt.Errorf("not logged in")
	}

	id, subscriber := p.subscribeTrackResponses()
	defer p.unsubscribeTrackResponses(id)

	cmd := fmt.Sprintf("**,imei:%s,%s", p.imei, command)
	if _, err := p.rw.Write([]byte(cmd)); err != nil {
		return err
	}

	if err := p.rw.Flush(); err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"id":       p.GetTrackingID(),
		"protocol": "gps103",
		"event":    "tx",
	}).Info(cmd)

	for {
		select {
		case trackResponse := <-subscriber:
			if trackResponse.Type == expectedResponseType {
				return nil
			}
		case <-time.After(3 * time.Second):
			return status.Error(codes.DeadlineExceeded, fmt.Sprintf("did not get confirmation for command %s, expected %s ", cmd, expectedResponseType.String()))
		}
	}
}

func (p *GPS103) TrackOnce() error {
	return p.sendCommand("100", pb.TrackerResponseType_LOCATION)
	// return p.sendCommand("B", pb.TrackerResponseType_LOCATION)
}

func (p *GPS103) TrackByTime(period pb.TrackingPeriod) error {
	periodString := period.String()
	i := periodString[1:len(periodString)] + periodString[0:1] // flip first letter to last position

	/*
		if err := p.sendCommand("C,"+i, pb.TrackerResponseType_LOCATION); err != nil {
			return err
		}
	*/
	return p.sendCommand("101,"+i, pb.TrackerResponseType_LOCATION)
}

func (p *GPS103) TrackByDistance(distance pb.TrackingDistance) error {

	distanceString := distance.String()
	i := distanceString[1:len(distanceString)] + distanceString[0:1] // flip first letter to last position

	return p.sendCommand("103,"+i, pb.TrackerResponseType_LOCATION)
}

func (p *GPS103) CancelTrack() error {
	return p.sendCommand("102", pb.TrackerResponseType_TRACKING_CANCELLED)
}

func (p *GPS103) CancelAlarm() error {
	return p.sendCommand("E", pb.TrackerResponseType_ALARM_CANCELLED)
}

// Arm sets ACC，door and shock alerts
func (p *GPS103) Arm() error {
	// return p.sendCommand("L", pb.TrackerResponseType_ARMED)
	return p.sendCommand("111", pb.TrackerResponseType_ARMED)
}

func (p *GPS103) Disarm() error {
	return p.sendCommand("112", pb.TrackerResponseType_DISARMED)
	// return p.sendCommand("M", pb.TrackerResponseType_DISARMED)
}

func (p *GPS103) ActivateLessGPRSMode() error {
	return p.sendCommand("118", pb.TrackerResponseType_LESS_GPRS_MODE_ACTIVATED)
	// return p.sendCommand("T", pb.TrackerResponseType_LESS_GPRS_MODE_ACTIVATED)
}

func (p *GPS103) DeactivateLessGPRSMode() error {
	return p.sendCommand("119", pb.TrackerResponseType_LESS_GPRS_MODE_DEACTIVATED)
	// return p.sendCommand("X", pb.TrackerResponseType_LESS_GPRS_MODE_DEACTIVATED)
}

// SetTimezone sets the offset, valid values are "-12" to "+12"
func (p *GPS103) SetTimezone(offset int) error {
	if offset < -12 || offset > 12 {
		return fmt.Errorf("invalid offset: valid range is -12 to 12")
	}
	offsetString := fmt.Sprint(offset)
	if offset > 0 {
		offsetString = "+" + offsetString
	}
	return p.sendCommand("108,"+offsetString, pb.TrackerResponseType_TIMEZONE_SET)
	// return p.sendCommand("I,"+offsetString, pb.TrackerResponseType_TIMEZONE_SET)
}
