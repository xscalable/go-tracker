package types

import (
	"gitlab.com/xscalable/go-tracker/pb"
)

type API struct {
	Addr string
}

type Config struct {
	API
}

type Protocol interface {
	// Getters
	GetConnectionID() string
	GetTrackingID() string
	GetLastStatusUpdate() *pb.StatusUpdate
	GetLastTrackerResponse() *pb.TrackerResponse
	GetTrackerResponseChan() chan *pb.TrackerResponse
	GetStatusUpdateChan() chan *pb.StatusUpdate

	// Commands
	TrackOnce() error
	TrackByTime(period pb.TrackingPeriod) error
	TrackByDistance(distance pb.TrackingDistance) error
	CancelTrack() error
	CancelAlarm() error
	Arm() error
	Disarm() error
	ActivateLessGPRSMode() error
	DeactivateLessGPRSMode() error
	SetTimezone(offset int) error
}

// Gateway defines the minimum contract our
// TCP server implementations must satisfy.
type Gateway interface {
	Run() error
	Close() error
	GetCurrentStatus() *pb.CurrentStatus
	GetCurrentLocations() *pb.CurrentLocations
	SubscribeStatusUpdates() (id string, c chan *pb.StatusUpdate)
	UnsubscribeStatusUpdates(id string)
	SubscribeTrackerResponses() (id string, c chan *pb.TrackerResponse)
	UnsubscribeTrackerResponses(id string)

	// Commands
	TrackOnce(trackID string) error
}
