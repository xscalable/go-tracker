//go:generate rm -rf pb/*.pb.go
//go:generate protoc --proto_path proto/ --go_out=plugins=grpc:pb proto/tracker.proto
package main

import (
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/xscalable/go-tracker/cmd"
)

var (
	version     = "development"
	buildstamp  = ""
	githash     = "local"
	environment = ""
)

func main() {
	if len(buildstamp) == 0 {
		buildstamp = time.Now().Format(time.RFC3339)
	}
	if len(environment) == 0 {
		if env := os.Getenv("GO_TRACKER_ENVIRONMENT"); len(env) > 0 && env != "production" {
			environment = env
			log.SetFormatter(&log.TextFormatter{})
		} else {
			environment = "production"
			log.SetFormatter(&log.JSONFormatter{})
		}
	}

	os.Setenv("GO_TRACKER_ENVIRONMENT", environment)
	os.Setenv("GO_TRACKER_BUILDSTAMP", buildstamp)
	os.Setenv("GO_TRACKER_GITHASH", githash)
	os.Setenv("GO_TRACKER_VERSION", version)

	cmd.Execute()
}
