package cmd

import (
	"context"
	"fmt"
	"io"
	"log"

	"github.com/golang/protobuf/jsonpb"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/spf13/cobra"
	"gitlab.com/xscalable/go-tracker/pb"
)

// helloCmd represents the hello command
var subscribe = &cobra.Command{
	Use:   "subscribe",
	Short: "subscribes the tracking stream",
	Long:  `This subcommand subscribes the tracking stream`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Subscribed tracking stream\n")
		conn, err := connect()
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		c := pb.NewTrackerClient(conn)
		ctx := context.TODO()
		// ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		// defer cancel()

		stream, err := c.Subscribe(ctx, &empty.Empty{})
		if err != nil {
			log.Fatalf("%v.SUBSCRIBE(_) = _, %v", c, err)
		}

		for {
			msg, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.SUBSCRIBE(_) = _, %v", c, err)
			}
			m := jsonpb.Marshaler{}
			js, err := m.MarshalToString(msg)
			if err != nil {
				fmt.Println(err)
				return
			}
			fmt.Printf("%s\n", js)
		}
	},
}

func init() {
	rootCmd.AddCommand(subscribe)
}
